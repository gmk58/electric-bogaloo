﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit_City : MonoBehaviour {

	public int maxBLocks;
	public List<GameObject> blocks;
	public bool canAdd = true;

	// Use this for initialization
	void Start () {
		blocks = new List<GameObject> ();
	}

	public void AddBlock(GameObject block) {
		if (!blocks.Contains (block) && canAdd) {
			blocks.Add (block);
			Hex_Block hex = block.GetComponent<Hex_Block> ();
			hex.city = this;
			hex.blockID = blocks.Count;
			if (blocks.Count >= maxBLocks) {
				canAdd = false;
			}
			hex.GenerateMissingNeighbors ();
		}
	}
}
