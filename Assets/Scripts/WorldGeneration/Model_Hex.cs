﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model_Hex : MonoBehaviour {

	public GameObject[] forks,elbows,straights,deadends,sixways;

	public GameObject GetRoadModel(Hex_Road.RoadType roadType) {
		switch (roadType) {
		case Hex_Road.RoadType.FORK:
			return forks [Random.Range (0, forks.Length)];
			break;
		case Hex_Road.RoadType.ELBOW_L:
			return elbows [Random.Range (0, elbows.Length)];
			break;
		case Hex_Road.RoadType.ELBOW_R:
			return elbows [Random.Range (0, elbows.Length)];
			break;
		case Hex_Road.RoadType.SIXWAY:
			return sixways [Random.Range (0, sixways.Length)];
			break;
		case Hex_Road.RoadType.STRAIGHT:
			return straights [Random.Range (0, straights.Length)];
			break;
		case Hex_Road.RoadType.DEADEND:
			return deadends [Random.Range (0, deadends.Length)];
			break;
		default:
			return deadends[0];
			break;
		}
	}
}