﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hex_Block : MonoBehaviour {

	public static string[] _RoadTypes = new string[] {"SIXWAY"};

	public bool forcedRoadType = false;
	public string roadType;
	public int blockID;
	public GameObject[] neighbors;
	public float blockSize;
	public LayerMask layerMask;
	public GameObject blockPrefab;
	public Unit_City city;

	public GameObject model;

	// Use this for initialization
	void Start () {
		ChooseRoadTypeRandom();
		GenerateMissingNeighbors ();
	}
	
	private void ChooseRoadTypeRandom() {
		//CheckNeighbors for fitting types;
		if (!forcedRoadType) {
			roadType = _RoadTypes [Random.Range (0, _RoadTypes.Length)];
		}
		UpdateModel ();
	}

	public void UpdateModel() {
		GameObject representativeModel = Instantiate (Model_Block._SharedInstance.GetModel (roadType));
		representativeModel.transform.parent = gameObject.transform;
		representativeModel.name = "Block "+blockID+"("+roadType+")";
		representativeModel.transform.position = transform.position;
		representativeModel.transform.rotation = transform.rotation;
		//model = representativeModel;
	}

	public void GenerateMissingNeighbors() {
		GatherNeighbors ();
		GenerateByType ();
	}

	public void GatherNeighbors() {
		neighbors = new GameObject[6];
		for (int i = 0; i < 6; i++) {
			neighbors [i] = null;
			Vector3 dir = Quaternion.AngleAxis (i * 60,transform.up) * transform.forward;
			RaycastHit hit;
			if (Physics.Raycast(transform.position,dir,out hit,blockSize,layerMask.value)) {
				neighbors [i] = hit.collider.gameObject;
			}
		}
	}

	public void GenerateByType() {
		switch (roadType) {
		case "STRAIGHT":
			GenerateBlockAt (0);
			GenerateBlockAt (3);
			break;
		case "FORK":
			GenerateBlockAt (1);
			GenerateBlockAt (3);
			GenerateBlockAt (5);
			break;
		case "SIXWAY":
			GenerateBlockAt (0);
			GenerateBlockAt (1);
			GenerateBlockAt (2);
			GenerateBlockAt (3);
			GenerateBlockAt (4);
			GenerateBlockAt (5);
			break;
		default:
			GenerateBlockAt (0);
			GenerateBlockAt (1);
			GenerateBlockAt (2);
			GenerateBlockAt (3);
			GenerateBlockAt (4);
			GenerateBlockAt (5);
			break;
		}
	}

	public void GenerateBlockAt(int index) {
		if (neighbors [index] == null && city.canAdd) {
			Vector3 dir = Quaternion.AngleAxis (index * 60, transform.up) * transform.forward * blockSize;
			GameObject newBlock = Instantiate (blockPrefab);
			newBlock.transform.position = transform.position + dir;
			newBlock.transform.rotation = Quaternion.LookRotation (dir);
			neighbors [index] = newBlock;
			city.AddBlock (newBlock);
		}
	}
}
