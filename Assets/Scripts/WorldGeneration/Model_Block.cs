﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model_Block : MonoBehaviour {

	public static Model_Block _SharedInstance;

	public GameObject[] sixway;
	public GameObject[] straight;
	public GameObject[] fork;

	// Use this for initialization
	void Start () {
		_SharedInstance = this;
	}
	
	public GameObject GetModel(string blockType) {
		switch(blockType) {
		case "STRAIGHT":
			return GetStraightModel ();
			break;
		case "FORK":
			return GetForkModel ();
			break;
		case "SIXWAY":
			return GetSixwayModel ();
			break;
		default:
			return GetSixwayModel ();
			break;
		}
	}

	private GameObject GetStraightModel() {
		int i = Random.Range(0,straight.Length);
		return straight [i];
	}

	private GameObject GetForkModel() {
		int i = Random.Range(0,fork.Length);
		return fork [i];
	}

	private GameObject GetSixwayModel() {
		int i = Random.Range(0,sixway.Length);
		return sixway [i];
	}
}
