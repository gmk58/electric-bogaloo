﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Car_Test : MonoBehaviour {

    public GameObject player;
    public bool isCop;
    public float refreshTime;
    public AudioSource backgroundMusic;
    private float count;
    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        StartCoroutine(WaitToStart());
    }

    private IEnumerator WaitToStart()
    {
        yield return new WaitForSeconds(3f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bike")
        {
            Debug.Log("Game Over");
            Time.timeScale = 0;
            backgroundMusic.Stop();
        }
    }

    // Update is called once per frame
    void Update () {
        count += Time.deltaTime;
        if (count>=refreshTime)
        {
            ChooseDestination();
            count = 0;
        }
	}

    private void ChooseDestination()
    {
        if (isCop)
        {
            agent.SetDestination(player.transform.position);
        }else
        {
            agent.SetDestination((player.transform.position-transform.position).normalized * -100);
        }
    }
}
