﻿		using System.Collections;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class AudioPeer : MonoBehaviour {
    AudioSource _audioSource;
    public static float[] _samples = new float[512];
    public static float[] _freqBand = new float[8];
    public static float[] _bandBuffer = new float[8];
    float[] _bufferDecrease = new float[8];

    public float[] _freqBandHighest = new float[8];
    public static float[] _audioBand = new float[8];
    public static float[] _audioBandBuffer = new float[8];

    public static float _Amplitude, _AmplitudeBuffer;
    float _AmplitudeHighest = 0;
    public float _audioProfile;

    // Use this for initialization
    void Start() {
        _audioSource = GetComponent<AudioSource>();
        AudioProfile(_audioProfile);
    }

    // Update is called once per frame
    void Update() {
        GetSpectrumAudioSource();
        MakeFrequencyBands();
        BandBuffer();
        CreateAudioBands();
        GetAmplitude();

    }

    //Gets Average Amplitude of all bands
    void GetAmplitude()
    {
        float _CurentAmplitude = 0;
        float _CurrentAmplitudeBuffer = 0;
        for (int i = 0; i < 8; i++) {
            _CurentAmplitude += _audioBand [i];
            _CurrentAmplitudeBuffer += _audioBandBuffer[i];
        }
        if (_CurentAmplitude > _AmplitudeHighest) {
            _AmplitudeHighest = _CurentAmplitude;
        }
        _Amplitude = _CurentAmplitude / _AmplitudeHighest;
        _AmplitudeBuffer = _CurrentAmplitudeBuffer / _AmplitudeHighest;
    }
    
    void AudioProfile(float audioProfile)
    {
        for (int i = 0; i < 8; i++)
        {
            _freqBandHighest[i] = audioProfile;
        }
    }


    void GetSpectrumAudioSource()
    {
        _audioSource.GetSpectrumData (_samples, 0, FFTWindow.Blackman);
    }


    void MakeFrequencyBands()
    {

        /*
         * 22050 / 512 = 43hz per sample
         * 
         * sub bass: 20hz - 60hz                    --subwoofer feel--
         * bass: 60hz - 250hz
         * low midrange: 250hz - 500hz
         * midrange: 500hz - 2,000hz
         * upper midrange: 2,000hz - 4,000hz
         * Pressense: 4,000hz - 6,000hz
         * Brilliance: 6,000hz - 20,000hz           --ouch--
         * 
         * 
         * 0 - 2 = 86hz
         * 1 - 4 = 172 hz - 87-258
         * 2 - 8 = 344hz - 259-602
         * 3 - 16 = 688hz -603-1,290
         * 4 - 32 = 1,276hz - 1,291-2,666
         * 5 - 64 = 2,752hz - 2,667-5,418
         * 6 - 128 = 5,504hz - 5,419-10,922
         * 7 - 256 = 11,008hz - 10,923-21,930
         *  510 
         */
        
        int count = 0;

            for (int i = 0; i < 8; i++) {


            float average = 0;
            int sampleCount = (int)Mathf.Pow(2, i) * 2;

            if (i == 7) {
                sampleCount += 2;
            }
            for (int j = 0; j < sampleCount; j++) {
                average = +_samples[count] * (count + 1);
                count++;
            }

            average /= count;

            _freqBand[i] = average * 10;
        }
    }

    void BandBuffer()
    {
        for (int g = 0; g < 8; ++g)
        {
            if (_freqBand [g] > _bandBuffer [g]) {
                _bandBuffer [g] = _freqBand [g];
                _bufferDecrease [g] = 0.005f;
            }

            if (_freqBand [g] < _bandBuffer [g]) {
                _bandBuffer [g] -= _bufferDecrease [g];
                _bufferDecrease [g] *= 1.2f;
            }
        }
    }

    void CreateAudioBands()
    {
        for (int i = 0; i < 8; i++)
        {
            if (_freqBand[i] > _freqBandHighest[i]) {
                _freqBandHighest[i] = _freqBand[i];
            }
            _audioBand [i] = (_freqBand [i] / _freqBandHighest[i]);
            _audioBandBuffer [i] = (_bandBuffer [i] / _freqBandHighest[i]);

        }
    }

    
}
