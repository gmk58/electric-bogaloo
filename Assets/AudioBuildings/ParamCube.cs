﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour
{

    public int _band;
    public float _startScale, _scaleMultiplier;
	public float ScaleVariation;
    public bool _useBuffer;
    Material _material;
    
	public bool _randomize_band = false;

    // Use this for initialization
    void Start()
    {
        _material = GetComponent<MeshRenderer>().materials[0];
		if (_randomize_band) {
			_band = Random.Range (0, AudioPeer._audioBand.Length - 1);
		}
		_scaleMultiplier += Random.Range(0,ScaleVariation) - ScaleVariation / 2f;
    }

    // Update is called once per frame
    void Update() {
        if (_useBuffer) { 
            transform.localScale = new Vector3(transform.localScale.x, (AudioPeer._audioBandBuffer[_band] * _scaleMultiplier) + _startScale, transform.localScale.z);
            Color _color = new Color (AudioPeer._audioBandBuffer[_band], AudioPeer._audioBandBuffer[_band], AudioPeer._audioBandBuffer [_band]);
            _material.SetColor("_EmissionColor", _color);
        }
        if (!_useBuffer) {
            transform.localScale = new Vector3(transform.localScale.x, (AudioPeer._audioBand[_band] * _scaleMultiplier) + _startScale, transform.localScale.z);
            Color _color = new Color(AudioPeer._audioBand [_band], AudioPeer._audioBand [_band], AudioPeer._audioBand [_band]);
            _material.SetColor("_EmissionColor", _color);
        }

    }
}