﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTriggerAmplitude : MonoBehaviour {
    public int _minInensity;
    public int _maxIntensity;
    Light _light;
	// Use this for initialization
	void Start () {
        _light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        _light.intensity = (AudioPeer._AmplitudeBuffer * (_maxIntensity - _minInensity)) + _minInensity;
	}
}
