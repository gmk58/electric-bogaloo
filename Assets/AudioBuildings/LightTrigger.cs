﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTrigger : MonoBehaviour {
    public int _band;
    public int _minInensity;
    public int _maxIntensity;
    Light _light;
	// Use this for initialization
	void Start () {
        _light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        _light.intensity = (AudioPeer._audioBandBuffer[_band] * (_maxIntensity - _minInensity)) + _minInensity;
	}
}
