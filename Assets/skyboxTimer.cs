﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skyboxTimer : MonoBehaviour {

    public float time = 120f;
    public float count = 0f;
    public float degrees = -180f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        count += Time.deltaTime;
        transform.rotation = Quaternion.Euler(Vector3.Lerp(new Vector3(0,0,0), new Vector3(degrees, 0, 0), count / time));
	}
}
